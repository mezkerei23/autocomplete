import React from 'react';
import './Autocomplete.css';
import List from '../components/List';
import * as moviesService from '../services/movies-service';

class Autocomplete extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      allMovieTitles: [],
      filteredMovieTitles: [],
      isListVisible: false,
      inputValue: '',
      selectedRowKey: -1,
    }
  }

  async componentDidMount() {
    try {
      const allMovieTitles = await moviesService.getMovieTitles();
      this.setState({
        allMovieTitles: allMovieTitles,
        filteredMovieTitles: allMovieTitles,
      });
    } catch (err) {
      console.log(err);
    }
  }

  handleOnInputClick = (e) => {
    e.stopPropagation();
    this.setState({isListVisible: true})
  };

  handleOnBackgroundClick = (e) => {
    this.setState({isListVisible: false})
  };

  updateInputValue = (value) => {
    this.setState({inputValue: value});
    this.filterTitles(value);
  };

  // we can optimize this by using Trie data structure
  filterTitles = (key) => {
    if (!key) {
      this.setState({
        filteredMovieTitles: this.state.allMovieTitles
      });
      return;
    }

    const titles = this.state.allMovieTitles.filter(item => item.title.includes(key.toLowerCase()));

    this.setState({
      filteredMovieTitles: titles
    });
  };

  handleSelectRow = (item) => {
    this.setState({selectedRowKey: item.id});
    this.updateInputValue(item.displayTitle);
  };

  render() {
    return (
      <div onClick={this.handleOnBackgroundClick}>
        <header className="Autocomplete-header">
          <p>
            Movie titles
          </p>
        </header>

        {/* I can use form with datalist which provides it's own autocomplete, but I think that's not the point of this test task :) */}
        <input
          onClick={this.handleOnInputClick}
          value={this.state.inputValue}
          onChange={e => this.updateInputValue(e.target.value)}
        />
        {this.state.isListVisible || this.state.inputValue.length > 0 ?
          <List titles={this.state.filteredMovieTitles}
                selectedRowKey={this.state.selectedRowKey}
                handleSelectRow={this.handleSelectRow}
                selectedText={this.state.inputValue}/>
          : null}
      </div>
    );
  }
}

export default Autocomplete;