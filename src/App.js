import React from 'react';
import './App.css';
import Autocomplete from './containers/Autocomplete';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>
            Find your next favorite movie
          </p>
          <Autocomplete/>
        </header>
      </div>
    );
  }
}

export default App;
