import React from 'react';
import './List.css';

class List extends React.Component {
  constructor(props) {
    super(props);
  }

  highlightText = (title) => {
    const index = title.toLowerCase().indexOf(this.props.selectedText.toLowerCase());
    if (!this.props.selectedText || index < 0 || this.props.selectedText.length === title.length) {
      return title;
    }

    return <>{title.slice(0, index)}
      <mark>{title.slice(index, index + this.props.selectedText.length)}</mark>
      {title.slice(index + this.props.selectedText.length)}</>;
  };

  render() {
    return (
      <ul>{this.props.titles.map(item =>
        <li className={item.id === this.props.selectedRowKey ? 'selected' : ''} key={item.id}
            onClick={() => this.props.handleSelectRow(item)}>{this.highlightText(item.displayTitle)}</li>
      )}</ul>
    )
  }
}

export default List;