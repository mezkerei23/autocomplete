import moviesDataProvider from '../movies-data-provider';

// I suppose that we can load all data into memory so no need for a paging
export const getMovieTitles = async () => {
  return moviesDataProvider.movies.map(film => {
    return {
      title: film.title.toLowerCase(),
      displayTitle: film.title,
      id: film.id,
    };
  });
};